import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { routerTransition } from './router.animations';

@Component({
  selector: 'iwdf-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  animations: [ routerTransition ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  getState(outlet) {
    console.log('outlet.activatedRouteData.state',outlet.activatedRouteData.state);
    return outlet.activatedRouteData.state;
  }
}
