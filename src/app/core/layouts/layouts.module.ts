import { NgModule }   from '@angular/core';
import { CommonModule }   from '@angular/common';
import { RouterModule } from '@angular/router';

import {
  BrandComponent,
  FooterComponent,
  HeaderComponent,
  MainComponent,
  MastheadComponent,
  NavComponent,
  PageComponent
} from './';



@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    BrandComponent,
    FooterComponent,
    HeaderComponent,
    MainComponent,
    MastheadComponent,
    NavComponent,
    PageComponent
  ],
  declarations: [
    BrandComponent,
    FooterComponent,
    HeaderComponent,
    MainComponent,
    MastheadComponent,
    NavComponent,
    PageComponent,
    NavComponent,
    BrandComponent,
    MastheadComponent
  ]
})
export class LayoutsModule { }
