import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent, ErrorComponent } from './pages';

import {
  ComponentsComponent,
  ComponentsHomeComponent
} from './modules/components';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full', data: { state: '' }},
  { path: 'components', loadChildren: './modules/components/components.module#ModulesComponentsModule',data: { state: 'components' }},
  { path: 'not-found', component: ErrorComponent, data: {message: 'Page not found!'} },
  { path: '**', redirectTo: '/not-found' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
