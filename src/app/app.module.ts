import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { LayoutsModule }     from './core/layouts/layouts.module';
import { PagesModule }     from './pages/pages.module';
import { ModulesComponentsModule }     from './modules/components/components.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    LayoutsModule,
    PagesModule,
    ModulesComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
