import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  OnDestroy,
  OnInit,
  QueryList,
  Renderer2,
  ViewEncapsulation } from '@angular/core';

import { ListItemComponent } from './list-item/list-item.component';

@Component({
  selector: 'iwdf-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  @ContentChildren(ListItemComponent) items: QueryList<ListItemComponent>
  constructor() { }

  ngOnInit() {
  }
  select(item) {
    console.log(item);
    console.log(this.items);
  }
}
