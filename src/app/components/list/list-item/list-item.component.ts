import { Component, OnInit, Inject, forwardRef} from '@angular/core';

import { ListComponent } from '../';

@Component({
  selector: 'iwdf-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  list: ListComponent;
  constructor(@Inject(forwardRef(() => ListComponent)) list: ListComponent) {
    this.list = list;
  }
  ngOnInit() {

  }
  onClickItem(event){
    console.log(event);
    this.list.select(this);
  }
}
