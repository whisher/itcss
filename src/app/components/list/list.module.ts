import { NgModule }   from '@angular/core';
import { CommonModule }   from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
    ListComponent,
    ListItemComponent

} from './';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports: [
    ListComponent,
    ListItemComponent
  ],
  declarations: [
    ListComponent,
    ListItemComponent
  ]
})
export class ListModule { }
