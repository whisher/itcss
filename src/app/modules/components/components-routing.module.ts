import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import {
  ComponentsComponent,
  ComponentsButtonsComponent,
  ComponentsGridComponent,
  ComponentsHomeComponent,
  ComponentsTypographyComponent
} from './';

const componentsRoutes: Routes = [
  { path: '', component:ComponentsComponent,
    children: [
      { path: '', component: ComponentsHomeComponent },
      { path: 'buttons', component: ComponentsButtonsComponent },
      { path: 'grid', component: ComponentsGridComponent },
      { path: 'typography', component: ComponentsTypographyComponent }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(componentsRoutes)
  ],
  exports: [RouterModule]
})
export class ComponentsRoutingModule {}
