import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsTypographyComponent } from './typography.component';

describe('TypographyComponent', () => {
  let component: ComponentsTypographyComponent;
  let fixture: ComponentFixture<ComponentsTypographyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsTypographyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsTypographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
