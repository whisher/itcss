import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsGridComponent } from './grid.component';

describe('GridComponent', () => {
  let component: ComponentsGridComponent;
  let fixture: ComponentFixture<ComponentsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
