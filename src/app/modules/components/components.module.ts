import { NgModule }   from '@angular/core';
import { CommonModule }   from '@angular/common';
import { RouterModule } from '@angular/router';

import { ComponentsRoutingModule } from './components-routing.module';

import {
  ComponentsComponent,
  ComponentsButtonsComponent,
  ComponentsGridComponent,
  ComponentsHomeComponent,
  ComponentsTypographyComponent
} from './';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ComponentsRoutingModule
  ],
  exports: [
    ComponentsComponent,
    ComponentsButtonsComponent,
    ComponentsGridComponent,
    ComponentsHomeComponent,
    ComponentsTypographyComponent
  ],
  declarations: [
    ComponentsComponent,
    ComponentsButtonsComponent,
    ComponentsGridComponent,
    ComponentsHomeComponent,
    ComponentsTypographyComponent

  ]
})
export class ModulesComponentsModule { }
