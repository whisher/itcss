import { ItcssPage } from './app.po';

describe('itcss App', () => {
  let page: ItcssPage;

  beforeEach(() => {
    page = new ItcssPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
